from pprint import pprint

import requests


url = 'https://dummyjson.com/products/{page}'

total_cost = 0
total_apple_cost = 0


for page in range(1, 101):

    product = requests.get(url.format(page=page)).json()

    product_price = product.get('price', 0)
    product_stock = product.get('stock', 0)
    product_cost = product_price * product_stock

    if product.get('brand').lower() == 'apple':
        total_apple_cost += product_cost

    total_cost += product_cost


pass
