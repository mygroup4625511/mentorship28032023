from pprint import pprint

import requests


url = 'https://dummyjson.com/products?limit=100'

# url_ukr_net = 'https://gordonua.com/ukr/news/war/genshtab-zsu-v-epitsentri-bojovih-dij-bilogorivka-bahmut-avdijivka-mar-jinka-1656876.html'
# response = requests.get(url_ukr_net)
# our_data = response.json()


response = requests.get(url)
our_products = response.json()

products = our_products.get('products', [])

total_cost = 0
total_apple_cost = 0


for product in products:
    picture_url = product['images']
    picture_data = requests.get(picture_url[0])
    with open(f'./images/{product.get("id", "")}-{product.get("brand", "")}.jpg', 'bw') as file:
        file.write(picture_data.content)

    product_price = product.get('price', 0)
    product_stock = product.get('stock', 0)
    product_cost = product_price * product_stock

    if product.get('brand').lower() == 'apple':
        total_apple_cost += product_cost

    total_cost += product_cost







pass
