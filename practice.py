"""
string = 'aaaaaaaaa' - True
string = 'bbbbbbbbbbb' - True
string = 'aaaaaaaaaabbbbbbbbbb' - True
string = 'aaaaababbbbbbbbbbbbb'     - False

"""
string1 = 'aaaaaaaaa'  # True
string2 = 'bbbbbbbbbbb'  # True
string3 = 'aaaaaaaaaabbbbbbbbbb'  # True
string4 = 'aaaaababbbbbbbbbbbbb'  # False

test = string3

if len(set(test)) == 1:
    print(True)
else:
    string_length = len(test)
    string_length_maybe_only_a = test.rstrip('b')
    set_a = set(string_length_maybe_only_a)
    if len(set_a) == 1:
        print(True)
    else:
        print(False)

